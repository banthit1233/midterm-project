const URL="https://api.themoviedb.org/3/";
const API_URL="api_key=712b63565e8beb18764c4ad6a185a576";
const IMG_URL="https:/image.tmdb.org/t/p/";
function checkForSearching(){
    if ($("#myselect").val()==1)
        searchMovie();
    else searchByActor();
}
async function searchByActor(){
    var inputMovie = $("input");
    if (!inputMovie.val()) return;
    $("#content").empty();
    $("#content").append(`
    <div class="col d-flex justify-content-center">
        <div class="spinner-grow text-primary" role="status">
            <span class="sr-only">Loading...</span>
        </div>
        <div class="spinner-grow text-danger" role="status">
            <span class="sr-only">Loading...</span>
        </div>
        <div class="spinner-grow text-warning" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    </div>

    `);
    const response= await fetch(`${URL}search/person?${API_URL}&query=${inputMovie.val()}`);
    const data= await response.json();
    console.log(data);
    $("#content").empty();
    if (data.total_results===0) alert("There is no result");
    count=0;
    for (const result of data.results){
        if (result.known_for_department==="Acting"){
            movies=await getActorMovie(result.id);
            console.log(movies);
            for (const movie of movies.cast){
                count+=1;
                $("#content").append(`
                <a class="col-block card p-1 m-1" onclick="getMovieDetails(${movie.id})" href="javascript:;" style="text-decoration:none;color:black">
                <div style="height:85%">
                    <img src="${IMG_URL}w300/${movie.poster_path}" class="card-img-top" alt="${movie.original_title}">
                </div>
                <div class="card-body">
                    <h5 class="card-title">${movie.original_title}</h5>
                    <p>Rating: ${movie.vote_average}</p>
                    <div class="progress" style="width:80%">
                        <div class="progress-bar progress-bar-striped bg-warning" role="progressbar" style="width: ${movie.vote_average*10}%" aria-valuenow="${movie.vote_average}" aria-valuemin="0" aria-valuemax="10"></div>
                    </div>
                </div>
                </a>
                `);
                if (count>=28) return;
            }
        }
    }
}
async function getTrending(){
    $("#content").empty();
    $("#content").append(`
    <div class="col d-flex justify-content-center">
        <div class="spinner-grow text-primary" role="status">
            <span class="sr-only">Loading...</span>
        </div>
        <div class="spinner-grow text-danger" role="status">
            <span class="sr-only">Loading...</span>
        </div>
        <div class="spinner-grow text-warning" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    </div>

    `);
    const response= await fetch(`${URL}trending/movie/week?${API_URL}`);
    const data= await response.json();
    console.log(data);
    $("#content").empty();
    for (const result of data.results){
        $("#content").append(`
        <a class="col-block card p-1 m-1" onclick="getMovieDetails(${result.id})" href="javascript:;" style="text-decoration:none;color:black">
        <div style="height:85%">
            <img src="${IMG_URL}w300/${result.poster_path}" class="card-img-top" alt="${result.original_title}">
        </div>
        <div class="card-body">
            <h5 class="card-title">${result.original_title}</h5>
            <p>Rating: ${result.vote_average}</p>
            <div class="progress" style="width:80%">
                <div class="progress-bar progress-bar-striped bg-warning" role="progressbar" style="width: ${result.vote_average*10}%" aria-valuenow="${result.vote_average}" aria-valuemin="0" aria-valuemax="10"></div>
            </div>
        </div>
        </a>
        `);
    }
}
async function searchMovie(){
    var inputMovie = $("input");
   
    if (!inputMovie.val()) return;
    $("#content").empty();
    $("#content").append(`
    <div class="col d-flex justify-content-center">
        <div class="spinner-grow text-primary" role="status">
            <span class="sr-only">Loading...</span>
        </div>
        <div class="spinner-grow text-danger" role="status">
            <span class="sr-only">Loading...</span>
        </div>
        <div class="spinner-grow text-warning" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    </div>

    `);
    const response= await fetch(`${URL}search/movie?${API_URL}&query=${inputMovie.val()}`);
    const data= await response.json();
    if (data.results.length==0) {
        alert("There is no result");
        getTrending();
    }
    console.log(data);
    $("#content").empty();
    for (const result of data.results){
        $("#content").append(`
        <a class="col-block card p-1 m-1" onclick="getMovieDetails(${result.id})" href="javascript:;" style="text-decoration:none;color:black">
            <div style="height:85%">
                <img src="${IMG_URL}w300/${result.poster_path}" class="card-img-top" alt="${result.original_title}">
            </div>
            <div class="card-body">
                <h5 class="card-title">${result.original_title}</h5>
                <p>Rating: ${result.vote_average}</p>
                <div class="progress" style="width:80%">
                    <div class="progress-bar progress-bar-striped bg-warning" role="progressbar" style="width: ${result.vote_average*10}%" aria-valuenow="${result.vote_average}" aria-valuemin="0" aria-valuemax="10"></div>
                </div>
            </div>
        </a>
        `);
    }

}
async function getMovieDetails(id){
    $("#content").empty();
    $("#content").append(`
    <div class="col d-flex justify-content-center">
        <div class="spinner-grow text-primary" role="status">
            <span class="sr-only">Loading...</span>
        </div>
        <div class="spinner-grow text-danger" role="status">
            <span class="sr-only">Loading...</span>
        </div>
        <div class="spinner-grow text-warning" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    </div>

    `);
    //Get movie detail
    const response= await fetch(`${URL}movie/${id}?${API_URL}`);
    const data= await response.json();
    console.log(data);
    if (data.status_code) 
        {
            alert(data.status_message);
            return;
        }
    //Get genres name 
   // const mapGenres=await getMapGenres();
        
    $("#content").empty();
    $("#content").append(`
    <div class="col-12 card p-3">
    <div class="row mb-3 mx-0">
        <div class="col-3">
            <img src="${IMG_URL}w300/${data.poster_path}" class="card-img-top" alt="${data.original_title}">
        </div>
        <div class="col-8">
            <h1>${data.original_title}</h1>
            <p style="opacity: 0.6">Release Date: ${data.release_date.substring(0,4)}</p>
            <h4>Overview</h4>
            <p class="lead">${data.overview}</p>
            <h4>Director</h4>
            <p class="lead"> ${await getDirector(id)}</p>
            
            <div id="genres">
                <h4>Genres</h4>
            </div>
            <h4>Lenght</h4>
            <p>${data.runtime} minutes</p>
            <h4>Rating</h4>
            <p> ${data.vote_average}</p>
            <div class="progress" style="width:50%">
                <div class="progress-bar progress-bar-striped bg-warning" role="progressbar" style="width: ${data.vote_average*10}%" aria-valuenow="${data.vote_average}" aria-valuemin="0" aria-valuemax="10"></div>
            </div>
           
        </div>
    </div>
    <nav>
    <div class="nav nav-tabs" id="nav-tab" role="tablist">
      <a style="font-size:20px" class="nav-item nav-link active" id="nav-cast-tab" data-toggle="tab" href="#nav-cast" role="tab" aria-controls="nav-cast" aria-selected="true">Cast</a>
      <a style="font-size:20px" class="nav-item nav-link" id="nav-review-tab" data-toggle="tab" href="#nav-review" role="tab" aria-controls="nav-review" aria-selected="false">Review</a>
    </div>
  </nav>
  <div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade show active" id="nav-cast" role="tabpanel" aria-labelledby="nav-cast-tab">
        <div id="actors" class="row mx-0">
        </div>
    </div>
    <div class="tab-pane fade" id="nav-review" role="tabpanel" aria-labelledby="nav-review-tab">
        <div id="reviews" class="row mx-0">
        </div>
    </div>
  </div>
       
    </div>
    `)
    for (const genre of data.genres){
        $("#genres").append(`
        <span class="btn btn-secondary">${genre.name} </span>
        `);
    }
    const credits = await getMovieCredit(id);
    console.log(credits.cast);
    for (const credit of credits.cast)
    {
        $("#actors").append(`
        <a class="bg-secondary text-white col-block card p-1 mb-2" onclick="getActorDetail(${credit.id})" href="javascript:;" style="text-decoration:none;color:black;width:20%">
            <div style="height:85%" >
                <img src="${IMG_URL}w300/${credit.profile_path}" class="card-img-top" alt="${credit.name}">
            </div>
            <div class="card-body">
                <h5>${credit.name}</h5>
                <p>${credit.character}</p>
            </div>
        </a>
        `);
    }
    const reviews= await getMovieReview(id);
    console.log(reviews);
    for (const review of reviews.results){
        $("#reviews").append(`
        <div class="col-md-12 shadow-sm p-3 mb-5 bg-white rounded">
            <div class="card-body">
                <h4 class="card-title">
                A review by ${review.author}
                </h4>
                <p>${review.content} </p>
            </div>
        </div>
        `)
    }
}
async function getMovieReview(id){
    const response= await fetch(`${URL}movie/${id}/reviews?${API_URL}`);
    const data= await response.json();
    return data;
}
async function getDirector(id){
    const credits = await getMovieCredit(id);
    dirName="Unknown";
    for (const credit of credits.crew)
    {
        if (credit.job == "Director")
            dirName = credit.name;
    }
    return dirName;
}
async function getMapGenres()
{
    const response= await fetch(`${URL}genre/movie/list?${API_URL}`);
    const data= await response.json();
    return data;
}
async function getMovieCredit(id){
    const response= await fetch(`${URL}movie/${id}/credits?${API_URL}`);
    const data= await response.json();
 
    return data;
}
async function getActorMovie(id){
    const response= await fetch(`${URL}person/${id}/movie_credits?${API_URL}`);
    const data= await response.json();
    return data;
}
async function getActorDetail(id){
    $("#content").empty();
    console.log(id);
    $("#content").append(`
    <div class="col d-flex justify-content-center">
        <div class="spinner-grow text-primary" role="status">
            <span class="sr-only">Loading...</span>
        </div>
        <div class="spinner-grow text-danger" role="status">
            <span class="sr-only">Loading...</span>
        </div>
        <div class="spinner-grow text-warning" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    </div>

    `);
    const response= await fetch(`${URL}person/${id}?${API_URL}`);
    const data= await response.json();
    console.log(data);
    if (data.status_code) 
        {
            alert(data.status_message);
            return;
        }
            
    $("#content").empty();
    $("#content").append(`
    <div class="col-12 card p-3">
        <div class="row mb-3 mx-0">
            <div class="col-3">
                <img src="${IMG_URL}w300/${data.profile_path}" class="card-img-top" alt="${data.name}">
            </div>
            <div class="col-8">
                <h1>${data.name}</h1>
                <p style="opacity: 0.6">Date of birth: ${data.birthday}</p>
                <h4>Biography</h4>
                <p>${data.biography}</p>
            </div>
        </div>
        <div class="row mx-0 mb-3">
            <div class="col-md-12">
                <h1>Movies</h1>
            </div>
        </div>
        <div id="movie" class="row mx-0">
        </div>
    </div>
    `)
    const movies= await getActorMovie(id);
    console.log(movies);
    for (const movie of movies.cast){
        $("#movie").append(`
        <div class="col-md-6 d-flex my-2">
            <a class="col-md-4" href="javascript:;">
                <img style="width:100%" src="${IMG_URL}w300/${movie.poster_path}" alt="${movie.original_title}" onclick="getMovieDetails(${movie.id})">
            </a>
            <div id="movie_detail_${movie.id}" class="col-md-8">
                <h4>${movie.original_title}</h4>
                <p>${movie.character}</p>
                
            </div>
        <div>
        `)
    
        if (!movie.release_date) 
            $(`#movie_detail_${movie.id}`).append(`
            <p class="lead">Release Date: Unknown</p>
            `)
        else 
            $(`#movie_detail_${movie.id}`).append(`
                <p class="lead">Release Date: ${movie.release_date}</p>
                `)
        if (movie.vote_average==0) 
            $(`#movie_detail_${movie.id}`).append(`
            <p class="lead">Rating: The movie is not yet rated</p>
            `)
            else  $(`#movie_detail_${movie.id}`).append(`
            <p class="lead">Rating: ${movie.vote_average}</p>
            <div class="progress" style="width:50%">
                <div class="progress-bar progress-bar-striped bg-warning" role="progressbar" style="width: ${movie.vote_average*10}%" aria-valuenow="${movie.vote_average}" aria-valuemin="0" aria-valuemax="10"></div>
            </div>
            `)
    }
 
}
function checkEnter(event)
{ 
    if (event.keyCode === 13)
    {
        $("button").click();
    }
}
$(document).ready(function() {
    getTrending();
});